%  TransferLearningPRPD.m
%    Implements the resnet18 pretrained network, trains the model based on a template_pool,
%    groups input pd data into prpd jpg renderings and runs inference and
%    other agregate calculations.  Results are output to a csv file
%   
%
%  CSV input expected to have the following fields as pulled from faspar
%  "FileName","uniqueID","phasenum","idfaspar","idfasphs","tvolt","pd_loc",
%  "pc_val","pd_phase","abs_ix","p1_mag","pd_flip","p2_abix","p2_xcl","p3_xcl",
%  "pd_rtt","p1_bcut","p2_num","pd_type","p1_rix","p2_rix","p3_rix","sosf_suc",
%  "pd_clip","pd_snr","pd_prb","sosf_upd","p1_wdth","pd_locgp","pd_phix",
%  "pd_ipix","ip_val","pd_gphix","pd_gipix","ip_gpval","pd_confi","sig_alph",
%  "p2_wdth","p3_wdth","PDseqprd","pd_lms_n","ml_pd","ml_confidence",
%  "manPD_type","pd_pattern","grp_splice"  
%  


clc
clear all
close all

% opening .csv file 
info           = what;                                    
pname_loc      = info.path;                             % path of executable

[fname, pname] = uigetfile('*.csv', 'select *.csv', pname_loc, 'multiselect', 'off');
if isequal(fname,0)||isequal(pname,0)
     return
else
     filename = strcat(pname, fname);
end

% First we need to delete any image data that is already in the folders
[status, message, messageid] = rmdir([pname 'images'], 's');

dn_class     = [pname 'images\'];                        % folder of images that will be classified
[status,msg] = mkdir(dn_class);                          % creating subfolder images
s_folder     = [dn_class 'selected'];
[status,msg] = mkdir(s_folder);


% Changing template pool to be hard coded this should always be the
% training folder pool and choosing wrong folder will result in bad data - SMP
dn_learn = [pname 'template_pool\'];

% Check to make sure we have a template_pool folder, if not launch dialogue
[status,msg] = mkdir(dn_learn);
if ~isequal(msg, 'Directory already exists.')
    dn_learn = uigetdir(dn_class, 'Template Pool not found, select(folder) for model training with [subfolders - 0: false, 1: true] ');
end

disp(['reading ' fname ' into memory...'])
%[num, txt]    = xlsread(filename);                       % reading into table
%label         = txt(2:end,:);
T      = readtable(filename);                            
%A      = table2array(T);
label = table2array(T(2:end,1));
uniqueid = table2array(T(2:end,2)); 
num   = table2array(T(2:end,3:end));

idphs  = num(:,3);% all ID phase (phase-ID)

all_phs = unique(idphs);                                  % all unique phase IDs

%  We have two potential ways to create the CSV files for ingenstion
%  unfortunatly minute variations lead to some wierd behavior sometimes
%  tables read in as cell arrays and sometimes double arrays, need to check
%  if cell arrays created and cast them, your notice we do this in several
%  spots
%
if iscell(all_phs)
    all_phs = str2double(all_phs);  %convert cell array to double array
end
    
% Now converting idphs to a double array
if iscell(idphs)
    idphs = str2double(idphs);
end

n_phs  = size(all_phs, 1);
disp([num2str(size(num,1)) ' PD events in this record'])
disp([num2str(n_phs) ' cable phases in this record'])

cnt = 0;
wb     = waitbar(0,'1','Name',' saving images...');
disp('saving images...')
for ix1 = 1:n_phs
    idx1 = find(idphs == all_phs(ix1,1));               % all records of the same phase ID
    if ~isempty(idx1)
        A_s    = num(idx1,:);
        gp     = A_s(:,27);                              % PD group index (1: near-end, 2: far-end, >=3: mid-span)
        all_gp = unique(gp);
        n_gp   = size(all_gp, 1);
       
        waitbar(ix1/n_phs,wb,['phase: ' num2str(ix1) '/' num2str(n_phs)])

        %Convert gp and all_gp to double array
        if iscell(gp)
            gp = str2double(gp);
        end
        if iscell(all_gp)
            all_gp = str2double(all_gp);
        end
        
        for ix2 = 1:n_gp
            idx2 = find(gp == all_gp(ix2,1));            % all records of same group
            if ~isempty(idx2)
                if iscell(A_s)
                    A_s = str2double(A_s);
                end    
                pn      = A_s(idx2(1,1),1); %str2double(A_s(idx2(1,1),1));              % phase number
                phs     = A_s(idx2,7); %str2double(A_s(idx2,7));                   % phase in degree
                pc      = A_s(idx2,6); %str2double(A_s(idx2,6));                   % discharge magnitude in pC
                vol     = A_s(idx2,4); %str2double(A_s(idx2,4));                   % test voltage
                loc     = A_s(idx2,5); %str2double(A_s(idx2,5));                   % PD location
                avg_pC  = round(mean(abs(pc)*10))/10;    % average pC val
                min_pC  = round(min(abs(pc)*10))/10;     % min. pC val
                avg_loc = round(mean(loc));              % average location
                min_vol = min(abs(vol));                 % min. test voltage
                splice_grp = A_s(idx2(1,1),44); %str2double(A_s(idx2(1,1),44));  % splice boolean  
                
                hf  = figure('visible','off');
                set(hf, 'MenuBar', 'none');
                set(hf, 'ToolBar', 'none');
                h   = plot(phs,pc,'ro','MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',4);
                xlim([0 360])
                ylim([-max(abs(pc)) max(abs(pc))])
                axis off
                set(hf,'color','w');
                
                saveas(hf,[dn_class...
                           label{idx1(1,1)} '$' uniqueid{idx1(1,1)} '_' num2str(pn) '_' num2str(all_phs(ix1,1)) '_' num2str(all_gp(ix2,1)) '_'...
                           num2str(avg_loc) '_' num2str(avg_pC) '_' num2str(min_pC) '_' num2str(min_vol) '_' num2str(splice_grp) '.jpg'])

                close(hf)
                cnt = cnt + 1;
            end
            
        end
        
    end
end
delete(wb)
disp([num2str(cnt) ' PRPD images saved'])

%% Transfer Learning: Phase Resolved PD Pattern Recognition

%For an general example of transfer learning with resnet, please see:
%https://www.mathworks.com/help/deeplearning/gs/get-started-with-transfer-learning.html

%% Configure

% Create a directory (pointed later to 'dn_learn') with two subfolders 1 & 0. Folder '1' contains examples
% with e-tree patterns, folder '0' will contain images that are not e-tree. 




% labeledImageDirectory = "PD phase plots labeled";
% 
% rootDir     = fullfile( fileparts( which('demoTransferLearningPRPD') ) ,".." );
% dsLocation  = fullfile( rootDir, "data", labeledImageDirectory );

%% Datastore 

% We'll create an image datastore (which is a pointer to files on disk) and
% specify the labelsource as the folder names (e.g. labels will be 1 and 0).
% The data are split into a training / test /validation split. Resnet
% requires the input images be [224 x 224], so we've added a resize function
% which is called each time an image is read from the datastore. 

disp(['training model on template pool...please wait'])
ds = imageDatastore( dn_learn, 'IncludeSubfolders', true, 'LabelSource', 'folderNames' );
[dsTrain, dsTest] = splitEachLabel( ds, 0.6, 0.2 ); %No validation split 

%Resize images to [224 x 224] for resnet
resizeFcn       = @(x)imresize(imread(x), [224,224] );
dsTrain.ReadFcn = resizeFcn;
dsTest.ReadFcn  = resizeFcn;

%% Load pretrained network 

% We use resnet18 as the pretrained network. You will need to download it
% from the Add-Ons menu item if it is not already installed. When we have
% more labeled data it is worth experimenting with other pretrained nets,
% such as inception. For transfer learning we update the final fully connected
% layer and classification layer.

net     = resnet18();
lGraph  = layerGraph( net );

%Update fully connect + classification layers 
nClasses        = numel( categories( dsTrain.Labels ) );
newFCLayer      = fullyConnectedLayer(nClasses,'Name','new_fc','WeightLearnRateFactor',10,'BiasLearnRateFactor',10);
newClassLayer   = classificationLayer('Name','new_classoutput');

lGraph = replaceLayer(lGraph,'ClassificationLayer_predictions',newClassLayer);
lGraph = replaceLayer(lGraph,'fc1000',newFCLayer);

%Original training params
% options = trainingOptions('sgdm', ...
%     'MiniBatchSize',5, ...
%     'MaxEpochs',10, ...
%     'InitialLearnRate',1e-4, ...
%     'Shuffle','every-epoch', ...
%     'Verbose',false, ...
%     'Plots','training-progress');

options = trainingOptions('sgdm', ...
    'MiniBatchSize',8, ...
    'ExecutionEnvironment','gpu', ...
    'MaxEpochs',16, ...
    'InitialLearnRate',1e-4, ...
    'Shuffle','every-epoch', ...
    'Verbose',false, ...
    'Plots','training-progress');

trainedNet = trainNetwork( dsTrain, lGraph, options );
disp(['training model ready...'])

% classifying
% _________________________________________________________________________
disp(['inference: classifying on ' num2str(cnt) ' PRPD images...please wait'])
dl              = imageDatastore(dn_class);
%Resize images to [224 x 224] for resnet
resizeFcn       = @(x)imresize(imread(x), [224,224] );
dl.ReadFcn      = resizeFcn;


[YPred,scores]  = classify(trainedNet, dl);
disp([num2str(length(YPred)) ' images classified...'])

% filenames and saving results
% _________________________________________________________________________
disp(['copying recognized images in folder and creating list...'])
sheet = {};

cnt = 1;
% Create the header row for output
sheet{cnt,1} = 'ProjectFile';
sheet{cnt,2} = 'UniqueID';
sheet{cnt,3} = 'phase_num';
sheet{cnt,4} = 'phaseID';
sheet{cnt,5} = 'group';    
sheet{cnt,6} = 'avg_loc';
sheet{cnt,7} = 'avg_pC';
sheet{cnt,8} = 'min_pC';
sheet{cnt,9} = 'PD IV';
sheet{cnt,10} = 'Etree score';
sheet{cnt,11} = 'Splice Grp';

cnt = cnt+1;
for ix = 1:length(YPred)
    if isequal(YPred(ix,1), categorical(1)) && scores(ix,2) >= 0.90
        str          = dl.Files{ix,1};                                            % full path and name (string) to image
        try
            copyfile(str,  s_folder);
        catch
            disp(['copying error at index: ' num2str(ix)])
        end
        %disp([num2str(dl.Files{ix,1}) ' _ ' num2str(scores(ix,2))])
        ix_          = find(str == '$');
        ix_slash      = find(str == '\');

        if ~isempty(ix_)
            if ~isempty(ix_slash)
                cname = str(1,ix_slash(1,end)+1:ix_ -1);                            % cable name (string)
                
                str1 = str(1,ix_+1 : end-4);                                       % coded portion of string (after $ sign)
                
                ix1_ = find(str1 == '_');
                if ~isempty(ix1_)
                    uniqid  = str1(1,1:ix1_(1,1) - 1);
                    pn      = str2double(str1(1,ix1_(1,1) + 1 : ix1_(1,2) - 1));   % phase number (A=1, B=2, C=3)
                    pid     = str2double(str1(1,ix1_(1,2) + 1 : ix1_(1,3) - 1));   % unique phase ID (reba database record) 
                    gid     = str2double(str1(1,ix1_(1,3) + 1 : ix1_(1,4) - 1));   % group number (1: near-end, 2: far-end, 3 or higher: mid-span)
                    avg_loc = str2double(str1(1,ix1_(1,4) + 1 : ix1_(1,5) - 1));   % average location of PD site [ft.]
                    avg_pC  = str2double(str1(1,ix1_(1,5) + 1 : ix1_(1,6) - 1));   % average absolute pC value [pC]
                    min_pC  = str2double(str1(1,ix1_(1,6) + 1 : ix1_(1,7) - 1));   % minimim abs pC value [pC]
                    min_vol = str2double(str1(1,ix1_(1,7) + 1 : ix1_(1,8) - 1));   % voltage at which PD first occured [kV]
                    splice_grp = str2double(str1(1,ix1_(1,8) + 1 : end));          % is this a splice group
                    
                    sheet{cnt,1} = cname;
                    sheet{cnt,2} = uniqid;
                    sheet{cnt,3} = pn;
                    sheet{cnt,4} = pid;
                    sheet{cnt,5} = gid;    
                    sheet{cnt,6} = avg_loc;
                    sheet{cnt,7} = avg_pC;
                    sheet{cnt,8} = min_pC;
                    sheet{cnt,9} = min_vol;
                    sheet{cnt,10} = scores(ix,2);
                    sheet{cnt,11} = splice_grp;
                    cnt = cnt+1;
                end
            end
        end
        

    end
end
disp([num2str(cnt) ' recordes identified. writing list...'])
%writecell(sheet,'list.csv','Delimiter',',','QuoteStrings',true)

filename2 = strcat(fname, '_output.csv');    %must end in csv
writetable( cell2table(sheet), filename2, 'writevariablenames', false, 'quotestrings', true);

